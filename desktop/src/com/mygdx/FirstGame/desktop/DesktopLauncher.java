package com.mygdx.FirstGame.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.FirstGame.Assets;
import com.mygdx.FirstGame.Bullet;
import com.mygdx.FirstGame.MainClass;

public class DesktopLauncher {
	

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "InDev - Galactic Warp"; // Setting the title
		config.height = 500; // Setting the height           
		config.width = 700; // Setting the width              
		config.useGL30 = true; // Use openGL 3.0
		config.resizable = false;
		config.addIcon("PlayerShipTest.png", Files.FileType.Internal); // Puts the player ship as the game icon. Use the internal directory
		
		
		new LwjglApplication(new MainClass(), config);
	}
}