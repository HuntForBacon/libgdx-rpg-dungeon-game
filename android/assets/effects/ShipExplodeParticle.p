Untitled
- Delay -
active: true
lowMin: 0.0
lowMax: 0.0
- Duration - 
lowMin: 200.0
lowMax: 456.0
- Count - 
min: 0
max: 197
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 333.0
highMax: 333.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 535.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.3
timelineCount: 3
timeline0: 0.0
timeline1: 0.93835616
timeline2: 1.0
- Life Offset - 
active: true
lowMin: 62.0
lowMax: 62.0
highMin: 155.0
highMax: 155.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 2.0
lowMax: 2.0
highMin: 4.0
highMax: 4.0
relative: false
scalingCount: 1
scaling0: 0.3137255
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: -10.0
lowMax: -10.0
highMin: -9.0
highMax: 263.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 360.0
highMin: 360.0
highMax: 0.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.5
timeline2: 1.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 180.0
highMax: 180.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Tint - 
colorsCount: 6
colors0: 0.8
colors1: 0.29411766
colors2: 0.0
colors3: 0.8
colors4: 0.34901962
colors5: 0.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.47368422
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2
timeline2: 0.72602737
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
- Image Path -
particle.png
