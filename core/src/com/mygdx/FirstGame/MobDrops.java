package com.mygdx.FirstGame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;

public class MobDrops {
	
	private ArrayList <MobDrops> droppedLootList = new ArrayList<MobDrops>(); // Holding the different loot
	
	private int dropChance;

	private float xPos;
	private float yPos;

	private Rectangle dropRectangle;
	
	public void dropItem(float x, float y){
		Random rand = new Random();
		int randomDrop = rand.nextInt(100);
		
		if(randomDrop > 0 && randomDrop < 20){
			droppedLootList.add(new Coin(x, y)); // Add the coin to the list at the x and y
		}
		if(randomDrop > 20 && randomDrop < 28){
			droppedLootList.add(new DoubleShotWeapon(x, y));
		}
		if(randomDrop > 40 && randomDrop < 50){
			droppedLootList.add(new LifePickupDrop(x, y)); // Add a life pickup to the list
		}
		if(randomDrop > 60 && randomDrop < 70){
			droppedLootList.add(new BombPickup(x, y));
		}
	}
	
	public MobDrops(){
		
	}
	
	public MobDrops(float x, float y){ // Use this as main constructor to set rectangles on subclasses
		this.xPos = x; // All child classes get their unique positions and can use the parent class to access them which is what the getX etc is for
		this.yPos = y;
		
		dropRectangle = new Rectangle(x, y, 32, 32);
	}
	
	public void renderDrops(OrthogonalTiledMapRenderer renderMap){ // Use the map render
		for(MobDrops drops : droppedLootList){ // The loop list
			if(drops instanceof Coin){ // If the drop is a coin		
				drops.setY(drops.getY() - 0.3f); // Set the position of the coin and make it fall
				((Coin) drops).coinUpdate(); // Call the coin update method
				drops.dropRectangle.set(drops.getX(), drops.getY(), 32, 32); // set each drop rectangle to the relevant position
				renderMap.getSpriteBatch().draw(((Coin) drops).getCoinCurrentFrame(), drops.getX(), drops.getY()); // Draw the coins on the map
			}
			
			if(drops instanceof DoubleShotWeapon){
				drops.setY(drops.getY() - 0.3f); // Set the position of the coin and make it fall
				((DoubleShotWeapon) drops).doubleShotUpdate(); // Call the update for the drop
				drops.dropRectangle.set(drops.getX(), drops.getY(), 32, 32); // set each drop rectangle to the relevant position
				renderMap.getSpriteBatch().draw(((DoubleShotWeapon) drops).getCurrentFrame(), drops.getX(), drops.getY());
			}
			
			if(drops instanceof LifePickupDrop){
				drops.setY(drops.getY() - 0.3f); // Set the position of the coin and make it fall
				((LifePickupDrop) drops).updateLifeAnimation(); // Update the animation
				drops.dropRectangle.set(drops.getX(), drops.getY(), 32, 32); // set each drop rectangle to the relevant position
				renderMap.getSpriteBatch().draw(((LifePickupDrop) drops).drawFrame(), drops.getX(), drops.getY()); // Draw the drop at the x and y
			}
			
			if(drops instanceof BombPickup){
				drops.setY(drops.getY() - 0.3f);
				((BombPickup) drops).update(); // Call the update from the subclass
				drops.dropRectangle.set(drops.getX(), drops.getY(), 32, 32); // set each drop rectangle to the relevant position
				renderMap.getSpriteBatch().draw(((BombPickup) drops).getCurrentFrame(), drops.getX(), drops.getY()); // Draw the pickup
			}
		}
	} 
	
	public void playerPickup(Player player){ // Checking if player will pick up the item
		
		Iterator<MobDrops> it = droppedLootList.iterator();
		
		while(it.hasNext()){
			MobDrops drop = it.next();
			
			if(drop.getItemBounds().overlaps(player.bounds)){ // If any mob drops overlap the player
				if(drop instanceof Coin){ // Is the drop a coin?
					it.remove(); // Remove the item so it doesn't render
					Assets.coinPickUp.play(1f); // Play the coin pickup sound
					player.addCoinScore(); // Adding coins to the score
				}
				
				if(drop instanceof DoubleShotWeapon){
					it.remove(); // Remove the drop
					player.setShotDouble(true); // Setting the double shot to true so that player can shoot 2 lots of bullets
				}
				
				if(drop instanceof LifePickupDrop){
					it.remove(); // Remove the drop
					player.setLife(player.getLives() + 1); // Add a life onto the player
					Assets.lifePickupSound.play(); // Play the sound
				}
				
				if(drop instanceof BombPickup){
					it.remove(); // Remove the pickup
					
					player.bombs.add((BombPickup) drop); // Add a bomb to the list
				}
			}
		}
	}
	
	
	public float getX() {
		return xPos;
	}

	public void setX(float x) {
		this.xPos = x;
	}

	public float getY() {
		return yPos;
	}

	public void setY(float y) {
		this.yPos = y;
	}
	
	
	public Rectangle getItemBounds(){
		return dropRectangle;
	}
}
