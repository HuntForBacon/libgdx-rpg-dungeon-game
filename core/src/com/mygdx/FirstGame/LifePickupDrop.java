package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class LifePickupDrop extends MobDrops{
	
	private TextureRegion[][] frames; // Store the different frames such as row and coloum position
	private Animation lifePickupAnimation; // The animation for the pickup
	private TextureRegion currentFrame; // Current frame
	private float animationTimer; // Animation timer

	public LifePickupDrop(float x, float y){
		super(x, y); // Call the constructor
		
		frames = TextureRegion.split(Assets.lifePickupSheet, 32, 32);
		
		lifePickupAnimation = new Animation(0.20f, frames[0]); // Create an animation that updates every 200 mili and uses the first row of the sprite sheet
	}
	
	public void updateLifeAnimation(){
		float delta = Gdx.graphics.getDeltaTime(); // Get the game time
		
		animationTimer += delta; // Add the delta to the timer
		
		currentFrame = lifePickupAnimation.getKeyFrame(animationTimer, true); // Get the timer and keep looping
	}
	
	public TextureRegion drawFrame(){ // Get the current frame
		return currentFrame;
	}
}
