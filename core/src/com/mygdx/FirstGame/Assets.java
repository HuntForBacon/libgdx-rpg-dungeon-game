package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Assets {
	
	protected static Texture bulletTexture;
	protected static Sprite bulletSprite;
	protected static Texture batSpriteSheet;
	protected static Sprite bulletSpriteFlip;
	protected static Texture batBulletTexture;
	protected static Sprite batBulletSprite;
	protected static Sound playerShoot;
	protected static Texture coinSpriteSheet;
	protected static ParticleEffect shipExplodeEffect;
	protected static ParticleEffect scoreParticle25;
	protected static Texture mainMenuBackground;
	protected static Sprite mainMenuSprite;
	protected static Sound mainMenuClick;
	protected static Texture mainTitle;
	protected static Sprite mainTitleSprite;
	protected static Music mainTitleMusic;
	protected static Texture gameHud;
	protected static Sprite hudSprite;
	protected static Sound coinPickUp;
	protected static Sound playerHit;
	protected static ParticleEffect enemyHitPlayerEffect;
	protected static Texture ufoDoubleShotSpriteSheet;
	protected static Music gameMusic;
	protected static Texture doubleShotPickUpSheet;
	protected static Texture lifePickupSheet;
	protected static Sound lifePickupSound;
	protected static Sound playerDoubleShot;
	protected static Texture bombPickupSheet;
	protected static ParticleEffect bombEffectParticle;
	protected static Sound bombSound;
	protected static ParticleEffect scoreParticle50;
	protected static ParticleEffect scoreParticle75;

	public static void load() {
		mainMenuBackground = new Texture(Gdx.files.internal("BackgroundMainMenu.png"));
		mainMenuBackground.setWrap(TextureWrap.Repeat, TextureWrap.Repeat); // Make the texture repeat all the time
		mainMenuSprite = new Sprite(mainMenuBackground); // Make a sprite
		mainMenuSprite.setSize(700, 500); // Resizing the sprite to fit the screen
		
		gameHud = new Texture(Gdx.files.internal("HUD.png"));
		hudSprite = new Sprite(gameHud);
		
		mainTitle = new Texture(Gdx.files.internal("spacetext.png"));
		mainTitleSprite = new Sprite(mainTitle);
		
		mainTitleMusic = Gdx.audio.newMusic(Gdx.files.internal("MainTitleMusic.mp3")); // Loading in the main title music
		
		bulletTexture = new Texture(Gdx.files.internal("BulletTest2.png")); // Load in the textures here so it only loads in once
		bulletSprite = new Sprite(bulletTexture); // Set the texture to the sprite
		
		bulletSpriteFlip = new Sprite(bulletTexture);
		bulletSpriteFlip.flip(false, true);
		
		batBulletTexture = new Texture(Gdx.files.internal("BatBullet.png"));
		batBulletSprite = new Sprite(batBulletTexture);
		
		batSpriteSheet = new Texture(Gdx.files.internal("UFOTest.png")); // Spritesheets that are turned into texture regions for animations
		coinSpriteSheet = new Texture(Gdx.files.internal("CoinTest2.png"));
		ufoDoubleShotSpriteSheet = new Texture(Gdx.files.internal("UfoDoubleShot.png"));
		
		// Textures for pickups
		
		doubleShotPickUpSheet = new Texture(Gdx.files.internal("DoubleShotPickUpSheet.png")); // Loading in the sprite sheet
		lifePickupSheet = new Texture(Gdx.files.internal("LifePowerupSheet.png"));
		bombPickupSheet = new Texture(Gdx.files.internal("BombPickupSheet.png"));
		
		// Pickup sounds
		
		lifePickupSound = Gdx.audio.newSound(Gdx.files.internal("LifePowerupSound.wav"));
		
		shipExplodeEffect = new ParticleEffect(); 
		shipExplodeEffect.load(Gdx.files.internal("effects/ShipExplodeParticle.p"), Gdx.files.internal("effects")); // Load in the particle and tell it where the default image for the particle location
		
		enemyHitPlayerEffect = new ParticleEffect();
		enemyHitPlayerEffect.load(Gdx.files.internal("effects/EnemyParticleHitPlayer"), Gdx.files.internal("effects"));
		
		bombEffectParticle = new ParticleEffect();
		bombEffectParticle.load(Gdx.files.internal("effects/BombExplodeEffect"), Gdx.files.internal("effects"));
		
		scoreParticle25 = new ParticleEffect();
		scoreParticle25.load(Gdx.files.internal("effects/25ScoreEffect"), Gdx.files.internal("effects"));
		scoreParticle50 = new ParticleEffect();
		scoreParticle50.load(Gdx.files.internal("effects/50ScoreEffect"), Gdx.files.internal("effects"));
		scoreParticle75 = new ParticleEffect();
		scoreParticle75.load(Gdx.files.internal("effects/75ScoreEffect"), Gdx.files.internal("effects"));
		
		
		playerShoot = Gdx.audio.newSound(Gdx.files.internal("Pew.mp3"));
		bombSound = Gdx.audio.newSound(Gdx.files.internal("BombSound.wav"));
		playerDoubleShot = Gdx.audio.newSound(Gdx.files.internal("DoubleShotSound.wav"));
		mainMenuClick = Gdx.audio.newSound(Gdx.files.internal("MenuClickSound.mp3"));
		coinPickUp = Gdx.audio.newSound(Gdx.files.internal("CoinPickup.mp3"));
		playerHit = Gdx.audio.newSound(Gdx.files.internal("PlayerHit.mp3"));
		gameMusic = Gdx.audio.newMusic(Gdx.files.internal("542284_ScienceBlasterAiler.mp3"));
	}
}
