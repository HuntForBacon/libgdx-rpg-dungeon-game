package com.mygdx.FirstGame;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

public class GameOverScreen implements Screen {
	
	private MainClass game;
	private BitmapFont font;
	private Skin skin;
	private Button playAgainButton;
	private TextButtonStyle buttonStyle;
	private Stage stage;
	private TextureAtlas buttonAtlas;
	private TextButton exitGameButton;
	private Label scoreLabel;
	private LabelStyle labelStyle;
	private Player player;
	private float timer2;
	private SpriteBatch batch;
	
	public GameOverScreen(MainClass mainClass){
		this.game = mainClass;
	}
	
	public GameOverScreen(MainClass mainClass, Player player){
		
		this.game = mainClass;
		this.player = player;
		batch = new SpriteBatch();
		
		font = new BitmapFont(Gdx.files.internal("font/font.fnt")); // Bitmap font using custom font
		
		skin = new Skin(); // Skin for the buttons
		stage = new Stage(); // Stage for the drawing and actors
		
		buttonAtlas = new TextureAtlas("Button.pack"); // Atlas to hold the button images
		
		skin.addRegions(buttonAtlas); // Add the regions to the skin so that they can be applied to the styles
		
		buttonStyle = new TextButtonStyle();
		buttonStyle.up = skin.getDrawable("button");
		buttonStyle.over = skin.getDrawable("buttonpressed");
		buttonStyle.font = font; // Set the style font
		
		playAgainButton = new TextButton("Play again", buttonStyle);
		playAgainButton.setSize(250, 80);
		playAgainButton.setPosition(Gdx.graphics.getWidth() / 2 - 100, Gdx.graphics.getHeight() / 2 ); // Setting the position of the button
		
		
		exitGameButton = new TextButton("Exit game", buttonStyle);
		exitGameButton.setSize(250, 80);
		exitGameButton.setPosition(Gdx.graphics.getWidth() / 2 - 100, Gdx.graphics.getHeight() / 2 - 120); // Setting the position of the button
		
		labelStyle = new LabelStyle();
		labelStyle.font = font;
		labelStyle.background = skin.getDrawable("button");
		
		scoreLabel = new Label("Score: ", labelStyle);
		scoreLabel.setAlignment(Align.center);
		scoreLabel.setSize(250, 80);
		scoreLabel.setPosition(Gdx.graphics.getWidth() / 2 - 100, Gdx.graphics.getHeight() / 2 + 150);
		
		Gdx.input.setInputProcessor(stage); // Sets the input and gets events such as clicks of buttons and things like rollovers
		
		playAgainButton.addListener(new InputListener(){ // Listener for button events
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { // Get the event for the touch down on the button
				
				Assets.mainMenuClick.play(); // Play the menu sound
				Assets.load();
				
				stage.addAction(Actions.sequence(Actions.fadeOut(2), Actions.run(new Runnable(){ // Make the stage actors fade out for 2 seconds and create a runnable that will happen after

					@Override
					public void run() { // Do this once the fade has taken place
						
						dispose(); // Manually call the dispose as changing screens doesn't do this
						game.setScreen(new GameScreen(game)); // Make a new game if the button is clicked
						
					}
					
				})));
				
				return true;
			}
		});
		
		exitGameButton.addListener(new InputListener(){
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				dispose();
				Gdx.app.exit(); // Shut the game down
				return true;
			}
			
		});
		
		stage.addActor(scoreLabel);
		stage.addActor(playAgainButton);
		stage.addActor(exitGameButton);
	}

	public void render(float delta) {
		Gdx.gl.glClearColor(0.95F, 0.95F, 0.95F, 0.95F);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); 
		
		try{
		scoreLabel.setText("Score: " + player.getPlayerScore());
		
		}catch(NullPointerException e){
			System.out.println("Error, score is null");
		}
		
		float timer = Gdx.graphics.getDeltaTime(); // Timer
		timer2 += timer * 0.2f; // Plus the timer and then * by 0.1f to slow it down
		
		if(timer2 > 1.0f){
			timer2 = 0f; // Reset the timer
		}
		
		Assets.mainMenuSprite.setV(timer2);    // Scrolls using the float values of 0-1f and sets it to the V
		Assets.mainMenuSprite.setV2(timer2+1); // Makes it scroll to the other side set by the timer and add the 1 for the scroll to act smoothly and to not rescale the image
											   // Also makes sure that the top and bottom move smoothly together
											   // By setting the timer2 + 1 this makes the background scroll the other way
		
		Assets.mainMenuSprite.setU(timer2);    // This makes the animation on the horizontal scroll to the right. timer2 -1 would make it scroll to the left
		Assets.mainMenuSprite.setU2(timer2+1); // Setting the U2 1 behind at all times so it moves nicely while it repeats
		
		batch.begin();
		Assets.mainMenuSprite.draw(batch);
		batch.end();
		
		stage.act(Gdx.graphics.getDeltaTime()); // Events taking place here such as rollovers etc
		stage.draw(); // Draw the stage
		
	}

	public void resize(int width, int height) {
		
	}

	public void show() {
		
	}

	public void hide() {
		
	}

	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() { // Disposing 
		stage.dispose();
		skin.dispose();
		buttonAtlas.dispose();
		Assets.mainMenuClick.dispose();
	}
}
