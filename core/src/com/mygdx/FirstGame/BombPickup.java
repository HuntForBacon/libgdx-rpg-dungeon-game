package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class BombPickup extends MobDrops {
	
	private TextureRegion[][] frames; // Store the frames
	private Animation animation; // Animation
	private TextureRegion currentFrame; // Current frame 
	private float animationTimer; // The timer for the animation

	public BombPickup(float x, float y){ // Get the x and y
		super(x,y); // Parent constructor
		
		frames = TextureRegion.split(Assets.bombPickupSheet, 32, 32); // Get the frames from the sprite sheet
		animation = new Animation(0.60f, frames[0]); // New animation that updates every 500 mili and uses the first row of the sprite sheet
	}
	
	public void update(){
		float delta = Gdx.graphics.getDeltaTime(); // Get the delta time
		
		animationTimer += delta; // Increase the timer
		
		currentFrame = animation.getKeyFrame(animationTimer, true); // Update using the timer and also loop the animation
		
	}
	
	public TextureRegion getCurrentFrame(){ // Return the current frame
		return currentFrame;
	}

}
