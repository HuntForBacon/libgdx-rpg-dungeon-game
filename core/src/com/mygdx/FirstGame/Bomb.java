package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class Bomb {
	
	private float x; // X pos of the bomb
	private float y; // X pos of the bomb
	protected Rectangle bounds; // Bounds for collisions
	private Animation bombAnimation; // Animation
	private TextureRegion[][] frames; // Store the frames
	private TextureRegion currentFrame; // Get the current frame
	private float bombAnimationTimer; // Animatiom timer 

	public Bomb(float x, float y){
		this.x = x;
		this.y = y;
		
		frames = TextureRegion.split(Assets.bombPickupSheet, 32 , 32);
		bombAnimation = new Animation(0.40f, frames[0]);
		
		bounds = new Rectangle(x, y, 32, 32);
	}
	
	public void updateBomb(){
		float delta = Gdx.graphics.getDeltaTime();
		
		bombAnimationTimer += delta;
		
		currentFrame = bombAnimation.getKeyFrame(bombAnimationTimer, false); // don't loop the animation
		
		setY(getY() + 2); // Update the y position
		
		bounds.set(getX(), getY(), 32, 32);
	}
	
	public boolean animationFinished(){ // Checking if the animation is finished
		return bombAnimation.isAnimationFinished(bombAnimationTimer);
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public float getY(){
		return y;
	}
	
	public Rectangle getBounds(){
		return bounds;
	}
	
	
	public TextureRegion getBombFrames(){
		return currentFrame;
	}

	public float getX() {
		return x;
	}
}
