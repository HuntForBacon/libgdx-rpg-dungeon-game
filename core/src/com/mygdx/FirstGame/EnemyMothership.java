package com.mygdx.FirstGame;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class EnemyMothership extends Entity {
	
	private float x;
	private float y;
	private TextureRegion[][] frames;
	private Animation animation;
	private TextureRegion currentFrame;
	private Vector2 direction;
	private float speed = 20;
	private float animationTimer;
	private float getBulletDelay;
	private Rectangle rec;
	
	public EnemyMothership(float x, float y){
		this.x = x;
		this.y = y;
		
		Random rand = new Random();
		
		speed = rand.nextInt(11) + 10; // Create a random speed between 10 and 20 for every different object
		
		frames = TextureRegion.split(Assets.ufoDoubleShotSpriteSheet, 32, 32);
		animation = new Animation(0.70f, frames[0]);
		
		rec = new Rectangle(x, y, 32, 32);
		
	}
	
	public void update(float playerX, float playerY, Entity bulletManager){
		
		direction = new Vector2(playerX - getX(), y);
		direction.nor();
		
		setX(x += direction.x * speed );
		setY(y -= 1.3f);
		
		float delta = Gdx.graphics.getDeltaTime();
		animationTimer += delta;
		getBulletDelay += delta;
		
		if(playerY + 400 > this.y && getBulletDelay > 0.70f * 3 && playerY < this.y - 80 ){ // If the posY of Ufo is smaller than than the player + 400, it can shoot and also make sure the time has passed. Also, if the y is bigger than the player it can shoot. This stops the enemy firing after going past
			
			bulletManager.addBullet(new Bullet(this.x + 10, this.y - 10, playerX, playerY)); // Adds the bullet to the array and takes in the x and y of the player as well as the player cords for the shooting
			
			getBulletDelay = 0; // Reset the delay for the bullet
		}
		
		currentFrame = animation.getKeyFrame(animationTimer, true);
		
		rec.set(x, y, 32, 32);
	}
	
	public Rectangle getBounds(){
		return rec;
	}
	
	public TextureRegion getCurrentFrame(){
		return currentFrame;
	}
	
	public float getX(){
		return x;
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public float getY(){
		return y;
	}
	
	public void setY(float y){
		this.y = y;
	}
}
