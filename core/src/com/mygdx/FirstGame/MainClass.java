package com.mygdx.FirstGame;

import com.badlogic.gdx.Game;

public class MainClass extends Game { // Game get an applicationlistener which wants a screen for the game
	
	private GameScreen gameScreen;
    private MainMenu mainMenu;
	private GameOverScreen test;

	@Override
	public void create () {
		
		Assets.load();
		
		gameScreen = new GameScreen(this); // Passing the MyGame class into the constructor of GameScreen to use the same game
		mainMenu = new MainMenu(this); // Is apart of the game
		//test = new GameOverScreen(this);
		
		//setScreen(gameScreen); // Setting the screen to the gameScreen using the listener
		setScreen(mainMenu);
		//setScreen(test); // Using this to test the game over screen
	}
}
