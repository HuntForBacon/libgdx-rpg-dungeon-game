package com.mygdx.FirstGame;
import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;


public class Bat extends Entity {
	
	private Sprite spriteBlock;
	private Rectangle batRect;
	
	private final Animation batAnimationRow1; // Animation
	protected TextureRegion currentFrame; // This is the current frame to draw
	private TextureRegion[][] batFrames; // This is holding the different animation tiles at the X and Y from the split
	float getTime; // Gets the delta time for updating the animation
	
	private int posX;
	private int posY;

	private float getBulletDelay;
	
	public Bat(int x, int y){ // Constructor means every bat object will get a x and y pos and also a new animation
		
		posX = x;
		posY = y;
		
		batFrames = TextureRegion.split(Assets.batSpriteSheet,Assets.batSpriteSheet.getWidth() / 4, Assets.batSpriteSheet.getHeight()); // This takes the texture will split the width which is 128 into 4 parts. 32, 32 can also be used but this is better. It also only gets height due to only have 1 row of sprites on the sheet
		batAnimationRow1 = new Animation(0.20f, batFrames[0]); // Frames 0 means to get the first row. This is useful if you have multiple rows on a sprite sheet so you could have second row as batFrames[1]
		// Every object gets it's own animation and rectangle
		batRect = new Rectangle(posX, posY, 32, 28); // Rectangle for collisions
		
	}
	
	public void update(float playerX, float playerY, Entity bulletManager){ // Pass in the entity so it uses the same one
		
		float oldPlayerY = playerY;
		
		float delta = Gdx.graphics.getDeltaTime(); // Get the delta time
		getTime += delta;
		getBulletDelay += delta;
		
		currentFrame = batAnimationRow1.getKeyFrame(getTime, true); // True means to keep looping here and currentFrame will be drawn at the GameScreen
			
		posY -= 0.2f;
		
		if(oldPlayerY + 400 > posY && getBulletDelay > 0.70f * 4 && playerY < posY - 80 ){ // If the posY of bat is smaller than than the player + 400, it can shoot and also make sure the time has passed. Also, if the posY is bigger than the player it can shoot. This stops the enemy firing after going past
			bulletManager.addBullet(new Bullet(posX + 10, posY - 10, playerX, playerY)); // Call the method for adding bullets
			getBulletDelay = 0;
		}
		
		batRect.set(getX(), getY(), 32, 32);
	}

	public Sprite getBlockImage(){
		return spriteBlock;
	}
	
	public Rectangle getRectangle(){
		return batRect; 
	}
	
	public TextureRegion getCurrentFrame(){ // Get the current frame
		return currentFrame;
		
	}
	
	public void setX(int x){
		this.posX = x;
	}

	public float getX(){
		return posX;
	}

	public float getY(){
		return posY;
	}

}
