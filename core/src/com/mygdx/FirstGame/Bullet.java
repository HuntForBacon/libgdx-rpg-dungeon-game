package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Bullet { // Every bullet created gets these from the constructor

	protected Sprite playerBulletSprite; // Every object will get it's own sprite
	protected Rectangle bulletCollision;
	
	public float getX; // Make private later
	public float getY;
	private Sprite batBulletSprite;
	private Vector2 direction;
	private float speed = 4; // Default bullet speed
	
	public Bullet(float x, float y, float pX, float pY){
		
		this.getX = x; // Every bullet gets a x and y
		this.getY = y;
		
		direction = new Vector2(pX - getX, pY - getY); // Create a vector that will point towards the player location. This takes away the bullet location from the player location to get where the player is and create a vector
		direction.nor(); // Normalise the vector which gives the vector a length of 1 but still contains the vector information on the direction which will keep all the bullets the same speed
		
		// The direction vector is good because it creates an invisible line from a location to another and this is useful for bullets
		
		playerBulletSprite = Assets.bulletSprite; // Grab the sprite from the Assets class and sets every object of bullet to the sprite. Each one will be unique
	    batBulletSprite = Assets.batBulletSprite; // Every bullet can have this sprite and gets a unique one and this is useful for the different bullet drawings
		
		bulletCollision = new Rectangle(x, y, 16, 16); // Every bullets gets a collision rec
		
	}
	
	public void bulletUpdate(){ // Used for updating the bullet collision rectangle
		bulletCollision.set(getX, getY, 16, 16); // Set the rectangle on the bullet x and y and the width and height
	}

	public void setY(float y) {
		this.getY = y;
	}
	
	public void shootAtPlayerLocation(){
		
		
		setY(getY += direction.y * speed ); // Set the bullet location to the direction of the player location (the direction vector) and then use the speed variable to speed up the bullet as the vector only has a length of 1. This case, it is 1 x 4 and always will be as this is the speed
		setX(getX += direction.x * speed );
		
	}
	
	public Sprite playerBullet(){ // Used for drawing player bullets
		return playerBulletSprite;
	}
	
	public Sprite batBullet(){
		return batBulletSprite;
	}

	public void setX(float x) {
		this.getX = x;
	}
}
