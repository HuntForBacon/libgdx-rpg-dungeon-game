package com.mygdx.FirstGame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Entity {
	
	private float getTime;
	protected ArrayList<Entity> SpawnArray = new ArrayList<Entity>();
	protected ArrayList<Bullet> spawnBatBulletList = new ArrayList<Bullet>();
	private float enemySpawnTimer;
	
	public void spawnEnemy(float f){
		
		int playerPos = (int) f; // Get the player Y cord
		
		float delta = Gdx.graphics.getDeltaTime();
		Random rand = new Random();
		int id = rand.nextInt(5);
		
		int enemyXPos = rand.nextInt(900 - 490) + 490; // Setting the x pos between the game bounds
		int enemyYPos = rand.nextInt(playerPos + 700 - playerPos + 500) + playerPos + 400; // Setting the spawn of enemies between a certain area of the y cord
		
		enemySpawnTimer += delta;
		
		if(enemySpawnTimer > 1.6f){ // If this time has passed then it will spawn a new enemy
			if(id == 1){
			SpawnArray.add(new Bat(enemyXPos, enemyYPos)); // Spawning bats
			}
			
			if(id == 2){
				SpawnArray.add(new UfoEnemyDoubleShot(enemyXPos, enemyYPos));
			}
			enemySpawnTimer = 0;
			
			if(id == 2){
				SpawnArray.add(new EnemyMothership(enemyXPos, enemyYPos));
			}
		}
	}
	
	public void renderBullets(OrthogonalTiledMapRenderer renderMap, ArrayList<Rectangle> collisonArray, Player player){ // Takes in the map
		
		for(Bullet bullet : spawnBatBulletList){ // For any bullets in the arraylist
			//bullet.setY(bullet.getY - 3); // Set the pos of bullets. It is slower than player bullets in this case. // Removed this so it now shoots at the player - see method below
			
			bullet.shootAtPlayerLocation();
			bullet.bulletUpdate(); // Calling the bullet update for the collision rectangle to be updated
			
			renderMap.getSpriteBatch().draw(bullet.batBullet(), bullet.getX, bullet.getY); // Draw the bullets
		}
		
		BulletCollisions(player, collisonArray ); // Call this method so it loops
	}
	
	public void BulletCollisions(Player player, ArrayList<Rectangle> collisonArray){ // Checking if any bullets collide with the player
		
		Iterator<Bullet> it = spawnBatBulletList.iterator(); // For removing bullets if they collide with player
		
		while(it.hasNext()){ // While there is another thing in the list
			Bullet bullet = it.next(); // Get the next thing in that list
			
			for(int i =0; i < collisonArray.size(); i++){
				if(bullet.bulletCollision.overlaps(collisonArray.get(i))){
					it.remove();
					break; // The break seems to stop a crash - Look into this
				}
			}
			
			if(bullet.bulletCollision.overlaps(player.bounds)){
				
				Assets.enemyHitPlayerEffect.start(); // Start the effect for the enemy bullet collisions
				Assets.enemyHitPlayerEffect.setPosition(bullet.getX, bullet.getY); // Make the effect happen at the bullet position
				
				player.removeLife(); // Remove a life
				Assets.playerHit.play(); // Play the hit sound
				
				it.remove(); // Remove the bullet
			}
		}
	}
	
	public void addBullet(Bullet Bullet){
		spawnBatBulletList.add(Bullet); // Adds bullet to the list
	}
}