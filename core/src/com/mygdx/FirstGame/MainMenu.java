package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class MainMenu implements Screen {

	private MainClass game; // Making sure the game uses the screen
	private float timer2; // Timer for background
	private OrthographicCamera cam; // Camera for the background
	private SpriteBatch batch; // Batch for drawing
	
	private TextureAtlas buttonAtlas; // TextureAtlas is good for putting lots of images together in the same file
	private TextButtonStyle buttonStyle; // The button style like colour and font etc
	private TextButton button; // The actual button for the screen
	private TextButton exitButton;
	
	private Skin skin; // Skin used for getting texture atlas and setting the button styles
	private Stage stage; // Used for all the buttons etc
	private BitmapFont font; // Load in a font
	private Image backgroundImage;
	private TextButton instructionsButton;

	public MainMenu(MainClass mainClass) {
		
		game = mainClass;
		
		stage = new Stage(); // Create a stage for the button events etc
		skin = new Skin(); // Create the skin for buttons etc
		font = new BitmapFont(Gdx.files.internal("font/font.fnt")); // Load in the custom bitmap font
		
		buttonAtlas = new TextureAtlas("button.pack"); // Get the pack of buttons made with the texture pack. The text must be right for the file format
		skin.addRegions(buttonAtlas); //Add the regions for the buttonAtlas into the skin
		
		buttonStyle = new TextButtonStyle();
		buttonStyle.up = skin.getDrawable("button"); // When nothing is happening then this button image is showing
		buttonStyle.over = skin.getDrawable("buttonpressed"); // If the mouse goes over the button, this image will be displayed
		buttonStyle.down = skin.getDrawable("buttonpressed"); // If clicked then the button style is this
		buttonStyle.font = font; // Set the buttonStyle font equals to the font which is the default font atm
		
		button = new TextButton("Play game", buttonStyle); // Make a button with the string text and use the style for the button, which is buttonStyle
		button.setSize(250, 80); // Setting the size of the button
		button.setPosition(Gdx.graphics.getWidth()/2 - 110, Gdx.graphics.getHeight()/2); // Setting the position of the button
		
		exitButton = new TextButton("Exit game", buttonStyle); // Exit game button
		exitButton.setSize(250, 80); 
		exitButton.setPosition(Gdx.graphics.getWidth() / 2- 110, Gdx.graphics.getHeight()/2 - 200 ); // Setting the position of the button
		
		instructionsButton = new TextButton("Instructions", buttonStyle);
		instructionsButton.setSize(250, 80);
		instructionsButton.setPosition(Gdx.graphics.getWidth() / 2- 110, Gdx.graphics.getHeight()/2 - 100 ); // Setting the position of the button
		
		stage.addActor(button); // Add button/actor to the stage so it can be on the screen and interact with events
		stage.addActor(exitButton);
		stage.addActor(instructionsButton);
		
		Gdx.input.setInputProcessor(stage); // Get the input processor and then tell it to set the input to the stage. This gets input such as touches and clicks but also needed for rollovers etc
		
		/* This input is called before every render which means it updates the buttons etc
		 * the input processor is what is called and then the listener will auto update due to the stage being called before the render loop
		 */
		button.addListener(new InputListener(){ // Add the button to the listener and make a new listener which is an anonymous class. Use this for button clicks etc
			
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { // Get the event, the button and the x and y of the mouse for the events
				Assets.mainMenuClick.play(); // Will gets the sound for clicking buttons
				
				stage.addAction(Actions.sequence(Actions.fadeOut(2), Actions.run(new Runnable(){ // This makes all the stage actors fade out for 1 second, after that it will then make a runnable which will change our screen
					
					public void run() { // Will run after the fade out action
						
						dispose(); // Call the dispose method auto as screens are never disposed on their own. Hide() is called instead
						game.setScreen(new GameScreen(game)); // If touched then the game will simply start as it switches screens
					}
					
				}))); // Anonymous class
				
				return true; // Return true for the touching of the button
			}
		});
		
		exitButton.addListener(new InputListener(){ // Button listener for exit button
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { // If the button is pushed down with the mouse position or tablet touch mouse
				Assets.mainMenuClick.play(); // Play click sound
				
				try {
					Thread.sleep(300); // Make the thread sleep for 300 mili so it plays the sound before the close
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				Gdx.app.exit(); // If the button is touched then the game will exit
				
				return true;
			}
		});
		
		instructionsButton.addListener(new InputListener(){
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				
				stage.addAction(Actions.sequence(Actions.fadeOut(2), Actions.run(new Runnable(){
					
					public void run() {
						
						dispose();
					
						game.setScreen(new InstructionScreen(game));
						
						
					}
					
				})));
		
				return true;
			}
		});
		
		batch = new SpriteBatch();
		
		cam = new OrthographicCamera();
		cam.setToOrtho(false, 700, 500);
	}

	public void render(float delta) {
		
		Gdx.gl.glClearColor(0.95F, 0.95F, 0.95F, 0.95F);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		cam.update(); // Update the cam
		
		batch.setProjectionMatrix(cam.combined);
		
		float timer = Gdx.graphics.getDeltaTime(); // Timer
		timer2 += timer * 0.2f; // Plus the timer and then * by 0.1f to slow it down
		
		if(timer2 > 1.0f){
			timer2 = 0f; // Reset the timer
		}
		
		Assets.mainMenuSprite.setV(timer2);    // Scrolls using the float values of 0-1f and sets it to the V
		Assets.mainMenuSprite.setV2(timer2-1); // Makes it scroll to the other side set by the timer and add the 1 for the scroll to act smoothly and to not rescale the image
		                                       // Also makes sure that the top and bottom move smoothly together
		stage.act(delta); // Call the stage act and basically gets updates and things such as rollovers
		
		batch.begin();
		
		Assets.mainMenuSprite.draw(batch); // Draw the sprite update
		batch.draw(Assets.mainTitleSprite, 45, 400); // Drawing the title
		
		batch.end();
		
		stage.draw(); // Drawing the stage on the screen. It doesn't need the spritebatch as it is it's own spritebatch
	}
	
	public void resize(int width, int height) {
		
	}
	
	public void show() {
		
		if(!Assets.mainTitleMusic.isPlaying()){ // Stops the music replaying if the user goes back to the main menu.
			Assets.mainTitleMusic.play(); // Play music on start
			Assets.mainTitleMusic.setLooping(true); // Loop the music
		}
	}

	public void hide() {
	}

	public void pause() {
		
	}

	public void resume() {
		
	}

	public void dispose() { // Disposing all the textures
		skin.dispose();
		stage.dispose();
		buttonAtlas.dispose();
		Assets.mainMenuClick.dispose();
	}

}
