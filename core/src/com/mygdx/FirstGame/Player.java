package com.mygdx.FirstGame;
import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


public class Player extends Sprite{ // Extend Sprite class because it has everything needed
	
	private Vector2 pos; // X and Y cords
	
	// The movement velocity
	private Vector2 velocity = new Vector2(); 
	private float speed = 60 * 2, gravity = 60 * 1.8f; // Speed and the gravity. Allowing downward force and then speed to overcome the force
	ArrayList<Bullet> bulletArray = new ArrayList<Bullet>(); // Hold an array of bullets
	protected ArrayList<BombPickup> bombs = new ArrayList<BombPickup>();
	protected ArrayList<Bomb> bombFired = new ArrayList<Bomb>();
	
	protected Rectangle bounds;

	private float getTime;
	private Rectangle bulletRec;
	private boolean bulletRemoved;

	private int lives;
	private int coinScore;

	private int score;

	protected boolean doubleShot;
	private float shootingTimer;

	private float bombTimer;
	private boolean removeBomb;
	private int bombAmount;



	public Player(Sprite sprite){ // This sprite
		super(sprite);
		
		lives = 5;
		coinScore = 0;
		score = 0;
		
		setY(550);
		setX(480);
		
		bounds = new Rectangle(getX(), getY(), 26, 28); // Set the collision to the x and y and set the width and height
	}
	
	public void draw(SpriteBatch spriteBatch){
		super.draw(spriteBatch);

	}
	
	public void update(float delta) {
		// Apply gravity 
		/*Gdx.graphics.getDeltaTime(); // Make sure that the gravity stays the same on all computers really
		
		velocity.y -= gravity * delta;
		
		// Clamp velocity and stop it falling too fast
		if(velocity.y > speed){
			velocity.y = speed; // Velocity can never go above speed and this stops it falling too fast
		}else if(velocity.y < -speed){
			velocity.y = -speed;
		}
		
		setX(getX() + velocity.x * delta); // Using the built in methods to get the position at every update and setting it to the velocity and delta
		setY(getY() + velocity.y * delta);*/

		/*if(Gdx.input.isKeyPressed(Keys.A)){ // Allows the players to move around
			setX(getX() + velocity.x - 3); // Setting the x by getting the X pos and then increasing the move
			
		}else if(Gdx.input.isKeyPressed(Keys.D)){ // Allows the players to move around
			setX(getX() + velocity.x + 3); // Setting the x by getting the X pos and then increasing the move
		}
		
		else if(Gdx.input.isKeyPressed(Keys.W)){
			setY(getY() + velocity.y + 3);
		}
		
		else if(Gdx.input.isKeyPressed(Keys.S)){
			setY(getY() + velocity.y - 3);
		}*/
		
		if(doubleShot){ // If the player has this ability
			float deltaShot = Gdx.graphics.getDeltaTime(); // Get the time
			shootingTimer += deltaShot; // Start the timer
			if(shootingTimer > 0.9f * 4){ // If the timer goes over the allowed time
				doubleShot = false; // Disable the ability
				shootingTimer = 0; // Reset the ability
			}
		}
	}
	
	public void resetPlayerCollision(){
		bounds.set(getX()-4, getY(), 26, 26 );
	
	}
	
	public void playerScroll(){
		setY(getY() + 0.4f); // Scrolling player up
	}
	
	public int getPlayerScore(){
		return score;
	}
	
	public void playerBulletCollide(Entity entity, MobDrops mobDrops){ // Entity allows checks for the array and mobDrop allows the drop of items
		
		Iterator<Bullet> it = bulletArray.iterator(); // Used an iterator to remove bullets from the list as an arraylist will crash
		
		while(it.hasNext()){ // While there is a bullet in the list, do this:
			
			Iterator<Entity> it2 = entity.SpawnArray.iterator(); // Must place this inside the bullet loop to make sure that every enemy is checked for every bullet, not just the first. This loops to the start of the list
			
			Bullet bullet = it.next(); //get the next Bullet in the list to test. bullet is a variable and can be called anything. This is basically a sort of for each loop
			
			bullet.setY(bullet.getY + 5 ); // Set the bullet pos to move up
			bullet.bulletCollision.set(bullet.getX, bullet.getY, 9, 16); // set collision the bullets for the update
			
			while(it2.hasNext()){	 // While there is an enemy
				Object obj = it2.next(); // Get the next enemy for checking
				if(obj instanceof Bat){ // If the obj is a bat 
					if(((Bat) obj).getRectangle().overlaps(bullet.bulletCollision)){ // If the bullet overlaps the bat
						bulletRemoved = true; // Remove bullet boolean to stop the if statement below executing
						
						Assets.shipExplodeEffect.start(); // If the player shoots the enemy then this effect will start
						Assets.shipExplodeEffect.setPosition(((Bat) obj).getX(), ((Bat) obj).getY()); // Set the pos to the obj that was killed
						Assets.scoreParticle25.start();
						Assets.scoreParticle25.setPosition(((Bat) obj).getX(), ((Bat) obj).getY());
						
						score += 25; // Add 25 points to the score
						
						mobDrops.dropItem(((Bat) obj).getX(), ((Bat) obj).getY()); // Will add the item to drop and gets the location of enemy
					    it.remove(); // Remove bullet
						it2.remove(); // Remove enemy	
						break; // Stop the loop
					}
				}
				
				if(obj instanceof UfoEnemyDoubleShot){ // If the obj is an instance of this class
					if(((UfoEnemyDoubleShot) obj).ufoBounds().overlaps(bullet.bulletCollision)){ // Does the bullet collide with the enemy?
						bulletRemoved = true; // Remove the bullet
						
						Assets.shipExplodeEffect.start(); // If the player shoots the enemy then this effect will start
						Assets.shipExplodeEffect.setPosition(((UfoEnemyDoubleShot) obj).getX(), ((UfoEnemyDoubleShot) obj).getY()); // Set the pos to the obj that was killed
						Assets.scoreParticle50.start();
						Assets.scoreParticle50.setPosition(((UfoEnemyDoubleShot) obj).getX(), ((UfoEnemyDoubleShot) obj).getY());
						
						score += 50; // Adds 50 to the score
						
						mobDrops.dropItem(((UfoEnemyDoubleShot) obj).getX(), ((UfoEnemyDoubleShot) obj).getY());
						it.remove(); // Remove the bullet
						it2.remove(); // Remove the entity
						break; // Stop the loop
					}
				}
				
				if(obj instanceof EnemyMothership){
					if(((EnemyMothership) obj).getBounds().overlaps(bullet.bulletCollision)){
						bulletRemoved = true;
						
						Assets.shipExplodeEffect.start(); // If the player shoots the enemy then this effect will start
						Assets.shipExplodeEffect.setPosition(((EnemyMothership) obj).getX(), ((EnemyMothership) obj).getY()); // Set the pos to the obj that was killed
						Assets.scoreParticle75.start();
						Assets.scoreParticle75.setPosition(((EnemyMothership) obj).getX(), ((EnemyMothership) obj).getY());
						
						score += 75;
						mobDrops.dropItem(((EnemyMothership) obj).getX(), ((EnemyMothership) obj).getY());
						
						it.remove();
						it2.remove();
						break;
					}	
				}
			}
			
			if(bullet.getY > getY() + 440 && !bulletRemoved){ // If the bullet is 440 above player position and not removed then do this
				it.remove();
			}
		}
		
		bulletRemoved = false; // Reset bullet state
	}
	
	public int getLives(){
		return lives;
	}
	
	public void removeLife() {
		
		lives--;
	}
	
	public void fireBomb(){
		float delta2 = Gdx.graphics.getDeltaTime();
		
		bombTimer += delta2; // Timer for the bomb delay
			
		if(Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)){ // If the left control is pressed
			
			Iterator<BombPickup> it = bombs.iterator();
			
			while(it.hasNext()){ // While the player has a bomb pickup
				
				BombPickup bomb = it.next(); // Get the next bomb pickup in the list
					
				if(bombTimer > 4f){ // If if timer has passed
					bombFired.add(new Bomb(getX(), getY())); // Make a new bomb and give it the player location
					bombTimer = 0; // Reset the timer
					
					it.remove(); // Remove the pickup
				}
			}
		}
	}
	
	public void updateBomb(Entity entity){ // Render the bombs
		
		Iterator<Bomb> it = bombFired.iterator(); // While there is a bomb
		
		while(it.hasNext()){ // Get the next bomb
			
			Bomb bomb = it.next(); // Get the next bomb in the list
			bomb.updateBomb(); // Update the bomb
				
			Iterator<Entity> it2 = entity.SpawnArray.iterator(); 
			
			while(it2.hasNext()){ // While there is next
				
				Object obj = it2.next(); // Get the next entity (object)
				
				if(bomb.animationFinished()){ // If the animation is finished
					if(obj instanceof Bat){ // If it is a bat (UFO)
						if(((Bat) obj).getY() < getY() ||((Bat) obj).getY() > getY()){ // If the object y is bigger than the players or smaller than the player
							it2.remove(); // Remove the enemy
							removeBomb = true; // Remove the bomb
							
							// No break as we want all the enemies removed, no just one
						}
					}
					
					if(obj instanceof UfoEnemyDoubleShot){
						if(((UfoEnemyDoubleShot) obj).getY() < getY() ||((UfoEnemyDoubleShot) obj).getY() > getY()){ // If the object y is bigger than the players or smaller than the player
							it2.remove();
							removeBomb = true;
						}
					}
				}
			}
			
			if(removeBomb == true){ // If true then the bomb will be removed
				Assets.bombEffectParticle.start(); // Start the particle effect
				
				Assets.bombEffectParticle.setPosition(bomb.getX(), bomb.getY()); // Set the location of the particle effect
				Assets.bombSound.play(); // Play the sound
				it.remove(); // Remove the bomb
			}
		}
		
		removeBomb = false; // Reset the bomb status
	}
	
	public void shootBullet(){
		
		float delta = Gdx.graphics.getDeltaTime(); // Get delta time
		
		getTime += delta; 
		
		if(Gdx.input.isKeyPressed(Keys.SPACE) || Gdx.input.isButtonPressed(Buttons.LEFT)){ // get the input from the space key and the mouse button - left click 
			
			if(getTime > 0.30f){ // If the time has passed over 200mili, add the bullet to the array for draw
				
				if(!doubleShot){
				bulletArray.add(new Bullet(getX() + 8, getY() + 25, 0, 0)); // Create the bullet and adds it to the player location and 25 ahead so that the bullets appear in front
				Assets.playerShoot.play(); // Play this sound
				getTime = 0; // Reset the time
				
				}
				
				if(doubleShot){
					bulletArray.add(new Bullet(getX() + 18, getY() + 25, 0, 0)); // Create the bullet and adds it to the player location and 25 ahead so that the bullets appear in front
					bulletArray.add(new Bullet(getX() - 5, getY() + 25, 0, 0)); // Create the bullet and adds it to the player location and 25 ahead so that the bullets appear in front
					Assets.playerDoubleShot.play(); // Play the double shot sound
					getTime = 0; // Reset the time
				}
			}
		}
	}

	public void addCoinScore() {
		coinScore++;
	}
	
	public int getBombs(){ // Get the bombs for the hud
		
		for(int i=0; i < bombs.size()+1; i++){ // Gets the size of the array and add 1 to it as arrays start at 0
			bombAmount = i; // Bomb amount is equal to i which is the amount of bombs
		}
		return bombAmount; // Return the bomb number
	}

	public int getCoins() {
		return coinScore;
	}

	public void setShotDouble(Boolean shot) { // Setting up the drop
		doubleShot = shot;
	}

	public void setLife(int life) {
		lives = life;
	}
}