package com.mygdx.FirstGame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Music.OnCompletionListener;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GameScreen implements Screen{
	
	private MainClass game;
	private SpriteBatch batch;
	private OrthographicCamera cam;
	private Player player;
	private PlayerHud playerHud;
	private Rectangle rec;
	
	private static TiledMap map;
	protected static OrthogonalTiledMapRenderer renderMap;
	
	private TiledMapTileLayer layer;
	
	private ArrayList<Rectangle> CollisonArray = new ArrayList<Rectangle>(); // Array to hold collisions
	private ArrayList<Rectangle> EntityCollision = new ArrayList<Rectangle>(); // Collisions for all enemies
	ArrayList<Bullet> removeAllBullets = new ArrayList<Bullet>();
	
	private static float tileWidth;
	private static float tileHeight;
	private float tx; 
	private float ty;
	
	private Bat bat1; // New bat image
	private Music music;
	private Cell cell; // Cell for the Tiled layer
	
	private boolean canMoveEast = true;
	private boolean canMoveWest = true;
	private boolean canMoveSouth = true;
	private boolean canMoveNorth = true;
	
	private Entity entity;
	private MobDrops mobDrops;
	
	private Bullet bullet;
	private OnCompletionListener musicChanger;
	
	public GameScreen(MainClass game){
		
		this.game = game; // Making it so this is the game and that it runs the update/render methods
		
		entity = new Entity();
		mobDrops = new MobDrops();
		player = new Player(new Sprite(new Texture("PlayerShipTest.png"))); // Create the player object which is also a sprite and add the texture
		playerHud = new PlayerHud(player);
		
		map = new TmxMapLoader().load("Map.tmx"); // Load in the map here like a texture
		renderMap = new OrthogonalTiledMapRenderer(map); // Telling the render(Sprite batch) to use the map as the sprite batch and this will mean that render.getSpriteBatch() will be needed, rather than batch = new SpriteBatch. Also pass in the map from Tiled here as it needed to be drawn and the camera has to set the view
		
		cam = new OrthographicCamera(); // Creating cam which is used for the tiles
		cam.setToOrtho(false, 700, 500); // Setting the cam bounds and to false!!! It must be false to stop the bad re-size and turn the game upside down
		
		batch = new SpriteBatch();
		
	}
	

	public void render(float delta) {
		
		Gdx.gl.glClearColor(0.95F, 0.95F, 0.95F, 0.95F);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		renderMap.setView(cam); // Set the cam to view the map! important
		renderMap.render(); // This will render the map from Tiled
		
		renderMap.getSpriteBatch().setProjectionMatrix(cam.combined); // This will basically make sure that everything is being scaled correctly and also rendering. Uses the cam cord system rather than default and does the math for me and tells the spritebatch to render according to the camera
		// Tells the batch the position of the drawn sprites must be determined by the camera's position
		
		player.update(delta); // For updating the player movement
		update();
		
		
		cam.position.set(700, player.getY() + 200, 0); // Make the cam follow the player. 0 is for the time frame. Usually would set cam to the xpos of the player but I want it to stay in one place for the X pos so it is 700
		cam.update(); // Cam updates etc
        
        renderMap.getSpriteBatch().begin(); // Using the same sprite batch and beginning the draw
      
        player.draw(renderMap.getSpriteBatch()); // This uses the same sprite batch as the map and will render the player on the same map etc
        
        for(Bullet bullet : player.bulletArray){ // For each Bullet in the array, render the bullets will take place (for each loop)
        	renderMap.getSpriteBatch().draw(bullet.playerBullet(), bullet.getX, bullet.getY); // Draw the bullet at the location
        }
        
        for(Bomb bombs : player.bombFired){
        	renderMap.getSpriteBatch().draw(bombs.getBombFrames(), bombs.getX(), bombs.getY());
        }
        
        for(Object obj : entity.SpawnArray){ // Using object as it allows any object
        	if(obj instanceof Bat){ // If obj is a bat
        		((Bat) obj).update(player.getX(), player.getY(), entity); // Gets the player x, y and passes in the entity for adding bullets
        		renderMap.getSpriteBatch().draw(((Bat) obj).getCurrentFrame(), ((Bat) obj).getX(), ((Bat) obj).getY()); // Drawing the bat
        	}
        	
        	if(obj instanceof UfoEnemyDoubleShot){ // If instance of Ufo
        		((UfoEnemyDoubleShot) obj).update(player.getX(), player.getY(), entity); // Update the ufo
        		renderMap.getSpriteBatch().draw(((UfoEnemyDoubleShot) obj).renderUfoDouble(), ((UfoEnemyDoubleShot) obj).getX(), ((UfoEnemyDoubleShot) obj).getY()); // Draw the ufo
        	}
        	
        	if(obj instanceof EnemyMothership){
        		((EnemyMothership) obj).update(player.getX(), player.getY(), entity);
        		renderMap.getSpriteBatch().draw(((EnemyMothership) obj).getCurrentFrame(), ((EnemyMothership) obj).getX(), ((EnemyMothership) obj).getY());
        	}
        }
        
		Assets.shipExplodeEffect.draw(renderMap.getSpriteBatch()); // Draw the effect
		Assets.shipExplodeEffect.update(delta); // Update the effects using the delta
		Assets.scoreParticle25.draw(renderMap.getSpriteBatch());
		Assets.scoreParticle25.update(delta);
		Assets.enemyHitPlayerEffect.draw(renderMap.getSpriteBatch());
		Assets.enemyHitPlayerEffect.update(delta);
		Assets.bombEffectParticle.draw(renderMap.getSpriteBatch());
		Assets.bombEffectParticle.update(delta);
		Assets.scoreParticle50.draw(renderMap.getSpriteBatch());
		Assets.scoreParticle50.update(delta);
		Assets.scoreParticle75.draw(renderMap.getSpriteBatch());
		Assets.scoreParticle75.update(delta);
        
    	entity.renderBullets(renderMap, CollisonArray, player); // Pass in the player for bullet detection
		mobDrops.renderDrops(renderMap); // Use the rendermap for the drawing
		
        renderMap.getSpriteBatch().end();
        
        playerHud.drawHUD(); // Draw the HUD on the screen using the stage/spritebatch
                
	}

	public void resize(int width, int height) {
		cam.viewportWidth = width; // Can be used for re-size
		cam.viewportHeight = height; // Used for re-size and also for allowing the camera to zoom in
	}
	
	public void update(){

		player.playerScroll();
		player.shootBullet();
		player.fireBomb();
		entity.spawnEnemy(player.getY());
		player.playerBulletCollide(entity, mobDrops); // Check for bullet collisions with enemies and passes in the entity
		player.updateBomb(entity); // Update the player bomb
		mobDrops.playerPickup(player); // Give the player bounds
		
		if(Gdx.input.isKeyPressed(Keys.D) && canMoveEast){ // If the user can move north then...
			player.bounds.set(player.getX()+4, player.getY(), 32, 32); // Set the bounds of the collision rectangle 4 ahead, stops player getting stuck in the wall
			for(int i =0; i < CollisonArray.size(); i++){ // Loop the array to check for collisions 
				if(player.bounds.overlaps(CollisonArray.get(i))){ // If collision then...
					canMoveEast = false; // Stops all movement 
					break; // break loop
				}
			}
			
			if(canMoveEast){ // If the loop runs all the way through and finds no collisions, the player can then move
				player.setX(player.getX() + 4); // Move the player
			}
		}
		
		if(Gdx.input.isKeyPressed(Keys.A) && canMoveWest){
			player.bounds.set(player.getX()-4, player.getY(), 32, 32);
			for(int i =0; i < CollisonArray.size(); i++){
				if(player.bounds.overlaps(CollisonArray.get(i))){
					canMoveWest = false;
					break;
				}
			}
			
			if(canMoveWest){
				player.setX(player.getX() - 4);
			}
		} 
		
		/*if(Gdx.input.isKeyPressed(Keys.W) && canMoveNorth){
			player.bounds.set(player.getX(), player.getY() + 2, 32, 32);
			for(int i =0; i < CollisonArray.size(); i++){
				if(player.bounds.overlaps(CollisonArray.get(i))){
					canMoveNorth = false;
					break;
				}
			}
			
			if(canMoveNorth){
				player.setY(player.getY() + 2);
			}
		} 
		
		if(Gdx.input.isKeyPressed(Keys.S) && canMoveSouth){
			player.bounds.set(player.getX(), player.getY() - 2, 32, 32);
			for(int i =0; i < CollisonArray.size(); i++){
				if(player.bounds.overlaps(CollisonArray.get(i))){
					canMoveSouth = false;
					break;
				}
			}
			
			if(canMoveSouth){
				player.setY(player.getY() - 2);
			}
		} */ // Decide if the user can move forward/back at a later time
		
		resetMovement(); // Make all movement reset for collision checks
		player.resetPlayerCollision(); // Reset the collision box to the players location
		
		if(player.getLives() <= 0){
			
			dispose();
			
			game.setScreen(new GameOverScreen(game, player));
		}
		
	}
	
	public void resetMovement(){ 
		canMoveWest = true;
		canMoveEast = true;
		canMoveSouth = true;
		canMoveNorth = true;
	}

	public void show() {
		
		layer = (TiledMapTileLayer) map.getLayers().get("Tile Layer 2"); // Get the layer from the map
		
		tileWidth = layer.getTileWidth(); // Get the width of the tiles
		tileHeight = layer.getTileHeight(); // Get the height of the tiles
		
		for (int y = 0; y < layer.getHeight(); y++) {
		   for (int x = 0; x < layer.getWidth(); x++) {
		      cell = layer.getCell(x, y); // Store the tiles in a cell
		      if (cell != null) { // If the cells are not null
		         TiledMapTile tile = cell.getTile(); // Create a tile variable here and grabs the tiles from the cells
		         if (tile != null && tile.getProperties().containsKey("Blocked")) { // If the cell contains the blocked keyword
		        	 
		             tx = x * tileWidth; // Take the x pos and * it by the width to get the position of the tile on the layer and places it on the screen
		             ty = y * tileHeight; // Take the x pos and * it by the height to get the position of the tile on the layer
		             
		             rec = new Rectangle(tx, ty, 32, 32); // Create a new rectangle and set it equal to the tx, ty, 32, 32.
		             
		             CollisonArray.add(rec); // Adds the rectangle to the array
		         }
		      }
		   }
	    }	
	}
	

	public void hide() {
		Assets.gameMusic.stop();
	}

	@Override
	public void pause() {
		
	}

	public void resume() {

	}

	public void dispose() { // Dispose the textures etc
		map.dispose();
		Assets.gameMusic.dispose();
		renderMap.dispose();
		player.getTexture().dispose();
		Assets.batSpriteSheet.dispose();
		Assets.coinPickUp.dispose();
		Assets.playerHit.dispose();
		Assets.bulletTexture.dispose(); 
		Assets.playerShoot.dispose();
	}

}