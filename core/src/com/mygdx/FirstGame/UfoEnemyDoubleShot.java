package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class UfoEnemyDoubleShot extends Entity{
	
	private float x;
	private float y;
	private Animation ufoAnimationRow1;
	private TextureRegion ufoTextureFrames[][];
	private Rectangle ufoBounds;
	private float animationTimer;
	private TextureRegion currentAnimationFrame;
	private float getBulletDelay;
	private float doubleShotDelay;
	
	public UfoEnemyDoubleShot(float x, float y){
		this.x = x; // This x pos of the ufo
		this.y = y; // Yypos of the ufo
		
		ufoTextureFrames = TextureRegion.split(Assets.ufoDoubleShotSpriteSheet, 32, 32); // Get the sprite sheet and split it
		ufoAnimationRow1 = new Animation(0.20f, ufoTextureFrames[0]); // Use the first row for the animation and update every 0.20f
		
		ufoBounds = new Rectangle(x, y, 32, 32); // Using a rectangle for collision bounds
		
	}
	
	public void update(float playerXPos, float playerYPos, Entity bulletManager){
		
		float delta = Gdx.graphics.getDeltaTime(); // Get the delta time
		animationTimer += delta;
		getBulletDelay += delta;
		
		currentAnimationFrame = ufoAnimationRow1.getKeyFrame(animationTimer, true); // Update the animation
		
		this.y -= 0.9f; // Move the enemy down the screen
		
		if(playerYPos + 400 > this.y && getBulletDelay > 0.70f * 2 && playerYPos < this.y - 80 ){ // If the posY of Ufo is smaller than than the player + 400, it can shoot and also make sure the time has passed. Also, if the y is bigger than the player it can shoot. This stops the enemy firing after going past
			
			bulletManager.addBullet(new Bullet(this.x, this.y - 10, playerXPos, playerYPos)); // Adds the bullet to the array and takes in the x and y of the player as well as the player cords for the shooting
			bulletManager.addBullet(new Bullet(this.x + 20, this.y - 10, playerXPos, playerYPos)); // Adding bullets to the list so that the enemies can shoot at the player
			
			getBulletDelay = 0; // Reset the delay for the bullet
		}
		
		ufoBounds.set(this.x, this.y, 32, 32); // Update the bounds of the ufo
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	public void setX(float x){
		this.x = x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public Rectangle ufoBounds(){
		return ufoBounds;
	}
	
	public TextureRegion renderUfoDouble(){ // Used for rendering the animation
		return currentAnimationFrame;
	}
}
