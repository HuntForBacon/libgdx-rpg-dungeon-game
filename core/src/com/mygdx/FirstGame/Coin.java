package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class Coin extends MobDrops {
	
	private TextureRegion[][] coinFrames;
	private Animation coinRow1;
	private float coinAnimationTimer;
	private TextureRegion currentCoinFrame;
	private Rectangle coinRectangle;
	
	public Coin(float x, float y){
		super(x, y); // Call the super class constructor so that it can use the x and y pos
		
		coinFrames = TextureRegion.split(Assets.coinSpriteSheet, 32, 32); // Split the texture into 32 by 32
		coinRow1 = new Animation(0.20f, coinFrames[0]); // Animation for row 1 of coins that updates every 20mili
		
	}
	
	public void coinUpdate(){
		float delta = Gdx.graphics.getDeltaTime();
		coinAnimationTimer += delta; // Timer for each animation step
		
		currentCoinFrame = coinRow1.getKeyFrame(coinAnimationTimer, true); // Current frame is every key frame at every 20mili and loop
		
	}

	public TextureRegion getCoinCurrentFrame(){ // Get the current frame
		return currentCoinFrame;	
	}
}
