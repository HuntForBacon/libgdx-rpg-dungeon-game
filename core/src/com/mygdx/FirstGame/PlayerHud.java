package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Align;


public class PlayerHud {
	
	private BitmapFont font;
	private Sprite hudDisplay = Assets.hudSprite;
	private Skin skin;
	private Stage stage;
	private Label livesLabel;
	private LabelStyle labelStyle;
	private Player player;
	private TextureAtlas hudAtlas;
	private Label coinScoreLabel;
	private Label playerScore;
	private Label bombLabel;

	
	public PlayerHud(Player player){
		font = new BitmapFont(Gdx.files.internal("font/font.fnt")); // Create a new font and grab the custom made font
		skin = new Skin(); // Create the skin for buttons etc
		stage = new Stage(); // Stage for drawing and actors/buttons etc and seeing if anything happens. Stage is used for INPUT event such as button clicked
		hudAtlas = new TextureAtlas("Hud.pack"); // Load in the Hud pack
		skin.addRegions(hudAtlas); // Add the hud pack to the skin
		font.setScale(1);
			
		this.player = player; // Use this player
		
		labelStyle = new LabelStyle(); // Making a style
		labelStyle.font = font; // The label font is the default font and this sets the instance variable to the font. setFont could be used here
		labelStyle.background = skin.getDrawable("SubHUD");
		
		livesLabel = new Label("Lives: " + player.getLives(), labelStyle); // Setting the label
		livesLabel.setSize(100, 50); // Set the label size so it can have a better text centre and this will also make the graphic bigger! This creates a bigger label. Default size is too small
		livesLabel.setAlignment(Align.center); // Setting the alignment of the text etc
		
		coinScoreLabel = new Label("Coins: " + player.getCoins(), labelStyle); // Make the coin label
		coinScoreLabel.setSize(100, 50);
		coinScoreLabel.setAlignment(Align.center);
		coinScoreLabel.setPosition(100, 0);
		
		playerScore = new Label("Score: " + 0, labelStyle); // Set the default value and give it the label style
		playerScore.setSize(200, 50); // Set the size
		playerScore.setAlignment(Align.center); // Align the text in the centre
		playerScore.setPosition(200, 0);
		
		bombLabel = new Label("Bombs: " + 0, labelStyle);
		bombLabel.setSize(100, 50);
		bombLabel.setAlignment(Align.center);
		bombLabel.setPosition(400, 0);
		
		stage.addActor(playerScore); // Add the player score to the stage
		stage.addActor(livesLabel); // Add the label to the stage/batch for drawing etc
		stage.addActor(coinScoreLabel); // Add the label
		stage.addActor(bombLabel);

	}
	
	public void drawHUD(){
		livesLabel.setText("Lives: " + player.getLives()); // Update the label text
		coinScoreLabel.setText("Coins: " + player.getCoins()); // Updating label
		playerScore.setText("Score: " + player.getPlayerScore());
		bombLabel.setText("Bombs: " + player.getBombs());
		stage.act();
		stage.draw(); // Draw the stage
	}
}
