package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;

public class InstructionScreen implements Screen{ // Implement Screen

	private MainClass game; // Get the game
	private Stage stage;
	private Skin skin;
	private TextureAtlas menuPack;
	private BitmapFont font;
	private TextFieldStyle textStyle;
	private Image bombImage;
	private TextButton backButton;
	private TextButtonStyle buttonStyle;
	private Image healthImage;
	private TextArea bombText;
	private TextArea healthText;
	private Image doubleShotImage;
	private TextArea doubleShotText;
	private TextArea title;
	private Image background;

	public InstructionScreen(MainClass mainGame) { // Passing in the game
		this.game = mainGame;
		stage = new Stage(); // Make the main stage
		font = new BitmapFont(Gdx.files.internal("font/font.fnt")); // Make a new font that is using the custom bitmap font
		skin = new Skin(); // Create a skin
		menuPack = new TextureAtlas("Menu.pack"); // Get the menu pack
		
		skin.addRegions(menuPack); // Add the buttons pack to the skin
		
		buttonStyle = new TextButtonStyle(); // Make a button style
		buttonStyle.font = font;
		buttonStyle.up = skin.getDrawable("button");
		buttonStyle.down = skin.getDrawable("buttonpressed");
		
		backButton = new TextButton("Back", buttonStyle);
		backButton.setSize(150, 40);
		backButton.setPosition(Gdx.graphics.getWidth() / 2 - 75 , Gdx.graphics.getHeight()/2 - 200); // Setting the position of the button
		
		textStyle = new TextFieldStyle(); // Make a text field style
		textStyle.font = font;
		textStyle.messageFont = font;
		textStyle.fontColor = Color.WHITE;
		
		background = new Image();
		background.setDrawable(skin, "MenuBackground");
		background.setPosition(0, 0);
		background.setSize(700, 500);
		
		bombImage = new Image(); // Make a new scene2D image
		bombImage.setDrawable(skin, "BombImageExample"); // Get the skin and the drawable
		bombImage.setPosition(10, 410);
		bombImage.setSize(70, 70);
		
		healthImage = new Image();
		healthImage.setDrawable(skin, "HealthImageExample");
		healthImage.setPosition(10, 310);
		healthImage.setSize(70, 70);
		
		doubleShotImage = new Image();
		doubleShotImage.setDrawable(skin, "DoubleShotImageExample");
		doubleShotImage.setPosition(10, 210);
		doubleShotImage.setSize(70, 70);
		
		bombText = new TextArea("Bombs: Bombs are useful for clearing every enemy on the screen in an impressive explosion. You get no XP or drops from enemies though!", textStyle); // Make a new text area
		bombText.setSize(400, 70);
		bombText.setPosition(100, 400);
		
		healthText = new TextArea("Health: Health packs grant 1 extra life. These are vital to any game, so make sure you collect as many as you can!", textStyle);
		healthText.setSize(400, 70);
		healthText.setPosition(100, 300);
		
		doubleShotText = new TextArea("Double shot: Gain the ability to shoot 2 bullets at once. Only lasts for a short period though!", textStyle);
		doubleShotText.setSize(400, 70);
		doubleShotText.setPosition(100, 200);
		
		Gdx.input.setInputProcessor(stage); // Make stage the input processor for things like button clicks
		
		
		backButton.addListener(new InputListener(){
			
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) { // If the button is clicked
				
				stage.addAction(Actions.sequence(Actions.fadeOut(2), Actions.run(new Runnable(){ // Make a sequence where the stage fades and then creates a runnable

				public void run() { // Run this
					
					dispose(); // Call the dispose
					game.setScreen(new MainMenu(game));
					
					} 
				
				})));
				
				return true;
			}
		});
		
		stage.addActor(background);
		stage.addActor(backButton); // Adding in the actors
		stage.addActor(bombText);
		stage.addActor(bombImage);
		stage.addActor(healthImage);
		stage.addActor(healthText);
		stage.addActor(doubleShotImage);
		stage.addActor(doubleShotText);
	}

	public void render(float delta) {
		Gdx.gl.glClearColor(0.95F, 0.95F, 0.95F, 0.95F);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(); // Calling the act
		stage.draw(); // Draw the stage on the screen
	}

	public void resize(int width, int height) {
		
	}

	public void show() {
		
	}

	public void hide() {
		
	}

	public void pause() {
		
	}

	public void resume() {
		
	}

	public void dispose() { // Dispose of un-needes stuff as this stops things like input processors being carried over to different screens
		stage.dispose();
		skin.dispose();
		menuPack.dispose();
		font.dispose();
	}

}
