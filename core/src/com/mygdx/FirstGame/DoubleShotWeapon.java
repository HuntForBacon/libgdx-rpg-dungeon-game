package com.mygdx.FirstGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class DoubleShotWeapon extends MobDrops { // Use the parent variables and methods
	
	private Animation doubleWeaponAnimation;
	private TextureRegion[][] frames;
	private TextureRegion currentFrame;
	private float animationTimer;
	
	
	public DoubleShotWeapon(float x, float y){ // Get the x and y positions
		super(x, y); // Call the parent constructor
		
		frames = TextureRegion.split(Assets.doubleShotPickUpSheet, 32, 32); // Split the texture using a texture region and store it in frames
		doubleWeaponAnimation = new Animation(0.10f, frames[0]); // Animation will update every 200 mili and will do the first row of the sprite sheet as that is row 0. 
		
	}
	
	
	public void doubleShotUpdate(){
		float delta = Gdx.graphics.getDeltaTime(); // Get the delta time
		
		animationTimer += delta; // Used for getting the time
		
		currentFrame = doubleWeaponAnimation.getKeyFrame(animationTimer, true); // Update will the animation timer and loop
	}
	
	public TextureRegion getCurrentFrame(){
		return currentFrame; // Get the current frame
	}

}
